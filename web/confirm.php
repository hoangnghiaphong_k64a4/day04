<?php
var_dump($_POST);
$full_name = $gender_user = $department = $date = $address = $fileupload = '';
if(!empty($_POST)) {
    if(isset($_POST['fullname']) && isset($_POST['gender']) && isset($_POST['department']) && isset($_POST['date']) &&  isset($_POST['address']) ) {
        $full_name = $_POST['fullname'];
        $department = $_POST['department'];
        $date = $_POST['date'];
        $address = $_POST['address'];
        $gender_user = $_POST['gender'];
    }
        $fileupload = basename($_FILES['fileupload']['name']);
    //Thư mục bạn sẽ lưu file upload
    $target_dir    = "uploads/";
    //Vị trí file lưu tạm trong server (file sẽ lưu trong uploads, với tên giống tên ban đầu)
    $target_file   = $target_dir.$fileupload;
    // Xử lý di chuyển file tạm ra thư mục cần lưu trữ, dùng hàm move_uploaded_file
    move_uploaded_file($_FILES["fileupload"]["tmp_name"],'C:/Users/Admin/OneDrive/Góc Tự Học/Thực tập Web/Week04/day04/web/uploads/'.$_FILES['fileupload']['name']);  
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    .infor {
        display: flex;
    }

    .label_form {
        max-width: 100px;
    }

    .form {
        line-height: 30px;
        margin-top: 8px;
    }

    .label_form {
        background-color: #499ede;
        margin-right: 20px;
        color: #fff;
        border: 2px solid #0b67ad;
    }

    .icon_red {
        color: red;
    }

    .notification {
        display: flex;
        justify-content: start;
        margin: 0;
        color: red;
        line-height: 20px;
        margin-left: 10px;
    }

    .infor_text {
        line-height: 34px;
    }

    .submit_confirm {
        pointer-events: none;
    }
    </style>
</head>

<body>
    <center>
        <div style="width:40% ;" class="web">
            <form method="post" action="confirm.php">
                <div class="infor form">
                    <label class="label_form" style="flex: 1">Họ và tên <span class="icon_red">*</span></label>
                    <p class="infor_text" style="margin: 0;"><?=$full_name?></p>
                </div>
                <div class="form" style=" display: flex;">
                    <label class="label_form" style="flex: 1">Giới tính <span class="icon_red">*</span></label>
                    <p class="infor_text" style="margin: 0;"><?=$gender_user?></p>
                </div>
                <div class="form" style="display: flex;">
                    <label style="flex:1 ;" class="label_form">Phân khoa <span class="icon_red">*</span></label>
                    <p class="infor_text" style="margin: 0;"><?=$department?></p>
                </div>
                <div class="form" style=" display: flex;">
                    <label class="label_form" style="flex: 1">Ngày sinh <span class="icon_red">*</span></label>
                    <p class="infor_text" style="margin: 0;"><?=$date?></p>
                </div>
                <div class="infor form" style="display: flex;">
                    <label class="label_form label_form" style="flex: 1">Địa chỉ</label>
                    <p class="infor_text" style="margin: 0;"><?=$address?></p>
                </div>
                <div class="form" style=" display: flex;">
                    <label class="label_form" style="flex: 1; max-height: 34px;">Hình ảnh</label>
                    <img src="<?=$target_file?>" alt="ảnh upload" style="width:250px;height:200px;">
                </div>
                <button class="submit_confirm"
                    style="margin-top: 20px;height: 44px; width: 120px; border-radius: 6px; border: 2px solid #0b67ad; background-color: #20b835; color: #fff; font-size: 15px;">Xác
                    nhận</button>
            </form>
        </div>
    </center>

</body>

</html>